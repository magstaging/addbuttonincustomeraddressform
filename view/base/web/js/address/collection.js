define([], function () {
    'use strict';

    return function (addressCollection) {
        const orig = addressCollection.defaults;
        orig.template = 'Mbs_AddButtonInCustomerAddressForm/collection';
        orig.buttonExtraLabel = 'Click Me';

        return addressCollection;
    }
});