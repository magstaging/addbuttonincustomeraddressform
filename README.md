# AddButtonInCustomerAddressForm

1. clone the repository

3. create folder app/code/Mbs/AddButtonInCustomerAddressForm when located at the root of the Magento site

4. copy the content of this repository within the folder

5. install the module php bin/magento setup:upgrade

6. login to the backend and verify the customer address form has a new button labelled 'Click Me'